<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Inscription</title>
</head>
<body>
    <h1>Inscription</h1>
    
    <form action="/inscription" method="post">
        <label for="fld_pseudo">Pseudonyme</label>
        <input type="text" id="fld_pseudo" name="pseudo" placeholder="Veuillez entrer votre pseudonyme">
        <label for="fld_prenom">Prénom</label>
        <input type="text" id="fld_prenom" name="prenom">
        <label for="fld_nom">Nom</label>
        <input type="text" id="fld_nom" name="nom">
        <label for="fld_pass">Mot de passe</label>
        <input type="password" id="fld_pass" name="pass">
        <input type="submit" value="Enregistrer">
    </form>
</body>
</html>
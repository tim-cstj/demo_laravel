<?php

class UserController extends BaseController {

	public function showInscription(){
        return View::make('form_inscription');
    }
    
    public function inscription(){
         //validation
        //ajout à la bd
        $user=new User();
        $user->pseudo=Input::get('pseudo');
        $user->prenom=Input::get('prenom');
        $user->nom=Input::get('nom');
        $user->password=Hash::make(Input::get('password'));
        $user->save();
        //redirection
        return Redirect::to('/');
    }
    
    public function viewProfile($pseudo){
        $users=User::where('pseudo','=',$pseudo)->get();
        
        return View::make('profil',array('user'=>$users[0]));
    }

}



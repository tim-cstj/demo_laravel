// --- INIT
var gulp = require('gulp'),
less = require('gulp-less'), // compiles less to CSS
minify = require('gulp-minify-css'), // minifies CSS
concat = require('gulp-concat'),
uglify = require('gulp-uglify'), // minifies JS
rename = require('gulp-rename'),
livereload = require('gulp-livereload');//browser refresh;

var paths = {
  'dev': {
    'less': './public/dev/less/',
    'scss': './public/dev/scss/',
    'js': './public/dev/js/',
    'vendor': './public/dev/vendor/'
  },
  'assets': {
    'css': './public/assets/css/',
    'js': './public/assets/js/',
    'vendor': './public/assets/bower_vendor/'
  }
};

// CSS
gulp.task('css', function() {
  return gulp.src(paths.dev.less+'style.less') // get file
  .pipe(less())
  .pipe(minify({keepSpecialComments:0}))
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest(paths.assets.css))
  .pipe(livereload());
});

// JS
gulp.task('js', function(){
  return gulp.src([
    paths.assets.vendor+'jquery/dist/jquery.js',
    paths.assets.vendor+'bootstrap/dist/js/bootstrap.js',
    paths.dev.js+'app.js'
    ])
    .pipe(concat('app.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(paths.assets.js))
    .pipe(livereload());
  });


// WATCH
//Rerun the task when a file changes
gulp.task('watch', function() {
  livereload.listen();
  gulp.watch(paths.dev.less+'*.less', ['css']);
  gulp.watch(paths.dev.js+'*.js', ['js']);
});
